#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import json
import requests
import time
__endpoint = "https://api.gandi.net/v5/email/"
__urls = {}

# Get the URL for an account
# We try to cache the result
def __get_url(apikey, domain, login):
    global __urls

    # Check if we already have the url for account
    if (login,domain) in __urls:
        return __urls[(login,domain)]

    # Get the URL for the account
    headers = {'authorization': 'Apikey %s' % apikey}
    r = requests.request("GET", __endpoint+'mailboxes/'+domain, headers=headers)
    if r.status_code == 200:
        accounts = json.loads(r.text)
        for account in accounts:
            if account['id'] and account['login'] == login and account['domain'] == domain:
                __urls[(login, domain)] = account['href']
                return __urls[(login, domain)]

    return None

# Get the Aliases for an account
def __get_aliases(apikey, domain, login):
    headers = {'authorization': 'Apikey %s' % apikey}
    if __get_url(apikey, domain, login):
        r = requests.request("GET", __get_url(apikey, domain, login) , headers=headers)
        if r.status_code == 200:
            info = json.loads(r.text)
            return info['aliases']
    return None

def list_account(apikey, domain, login, sort=False):
    print(login + '@' + domain + ':')
    aliases = __get_aliases(apikey, domain, login)
    if aliases:
        if sort:
            aliases.sort()
        if len(aliases) > 0:
            for alias in aliases:
                print("\t" + alias)
        else:
            print("\t(No alias)")
    else:
        print("\t(error getting aliases)")
    print('')

def add_alias(apikey, domain, login, alias, sort=False):
    aliases = __get_aliases(apikey, domain, login)
    if not alias in aliases:
        print('Adding ' + alias + ' in ' + login + '@' + domain)
        aliases.append(alias)
        headers = {'authorization': 'Apikey %s' % apikey, 'content-type': 'application/json'}
        payload = json.dumps({"aliases":aliases})
        r = requests.request("PATCH", __get_url(apikey, domain, login), data=payload, headers=headers)
    else:
        print(alias + ' exists in ' + login + '@' + domain)
    print('')
    time.sleep(5)
    list_account(apikey, domain, login, sort)

def remove_alias(apikey, domain, login, alias, sort=False):
    aliases = __get_aliases(apikey, domain, login)
    if alias in aliases:
        print('Removing ' + alias + ' in ' + login + '@' + domain)
        aliases.remove(alias)
        headers = {'authorization': 'Apikey %s' % apikey, 'content-type': 'application/json'}
        payload = json.dumps({"aliases":aliases})
        r = requests.request("PATCH", __get_url(apikey, domain, login), data=payload, headers=headers)
    else:
        print(alias + ' does not exists in ' + login + '@' + domain)
    print('')
    time.sleep(5)
    list_account(apikey, domain, login, sort)


# Execute as a command
if __name__ == '__main__':
    # Some import required in this case
    import os
    import argparse

    parser = argparse.ArgumentParser()
    # Optional arguments
    parser.add_argument("-f", "--file", dest="rcfile", default=os.getenv("HOME")+"/.gandimailalias.rc", help="Setting file")
    parser.add_argument("-d", "--debug", dest="debug", action="store_true", help="Show debug messages")
    parser.add_argument("-s", "--sort", dest="sort", action="store_true", help="Sort alias alphabeticaly")
    # List command
    subparsers = parser.add_subparsers(dest="action", title="Available actions")
    parser_list = subparsers.add_parser("list", help="List aliases")
    parser_list.add_argument("account", nargs="?", help="Limit output to account")
    # Add command
    parser_add = subparsers.add_parser("add", help="Add alias")
    parser_add.add_argument("account", help="Account where to add the alias")
    parser_add.add_argument("alias", help="Alias to be added")
    # Remove command
    parser_remove = subparsers.add_parser("remove", help="Remove alias")
    parser_remove.add_argument("account", help="Remove an alias from an account")
    parser_remove.add_argument("alias", help="Alias to be removed")
    # Delete command
    parser_delete = subparsers.add_parser("delete", help="Same as remove")
    parser_delete.add_argument("account", help="Remove an alias from account")
    parser_delete.add_argument("alias", help="Alias to be removed")
    # Parse arguments
    args = parser.parse_args()

    # Read a config file (JSON formated)
    settings = None
    try:
        settings_f = open(args.rcfile, 'r')
        settings = json.load(settings_f)
        settings_f.close()
        if args.debug:
            print('settings: ' + str(settings))
    except FileNotFoundError:
        print('Settings file not found: %s' % args.rcfile)
        exit(1)
    except json.decoder.JSONDecodeError:
        print('Error parsing settings file: %s' % args.rcfile)
        exit(1)

    # Get the apikey, domain and login for an account
    def __get_account(account):
        if account in settings:
            apikey = settings[account]
            login, domain = account.split('@')
            if args.debug:
                print('login: ' + login + '\tdomain: ' + domain + '\tapikey: ' + apikey)
            if login and domain and apikey:
                return apikey, login, domain
            else:
                return None, None, None
        else:
            print('Account %s not found or misconfigured' % args.account)
            exit(1)

    if args.account:
        apikey, login, domain = __get_account(args.account)

    # Get the action
    if args.action == 'list':
        if args.account:
            list_account(apikey, domain, login, args.sort)
        else:
            for account in settings:
                apikey, login, domain = __get_account(account)
                if apikey and login and domain:
                    list_account(apikey, domain, login, args.sort)
    elif args.action == 'add':
        add_alias(apikey, domain, login, args.alias, args.sort)
    elif args.action == 'remove' or args.action == 'delete':
        remove_alias(apikey, domain, login, args.alias, args.sort)

# vim: set ts=4 sw=4 tw=0 et :
